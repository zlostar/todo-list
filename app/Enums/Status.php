<?php

namespace App\Enums;

enum Status: string {
    case OPEN = 'open';
    case DISABLED = 'disabled';
    case UNCOMPLETED = 'uncompleted';
    case COMPLETED = 'completed';
}
