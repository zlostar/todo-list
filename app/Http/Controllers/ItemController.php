<?php

namespace App\Http\Controllers;

use App\Traits\ItemRulesTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Enums\Status;
use Illuminate\Validation\Rules\Enum;

class ItemController extends Controller
{
    use ItemRulesTrait;
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, string $todo_list_id)
    {
        return Item::where('todo_list_id', $todo_list_id)->
                    orderBy('created_at', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, string $todo_list_id)
    {
        $newItem = new Item;
        $newItem->name = $request->item['name'];
        $newItem->todo_list_id = $todo_list_id;
        $newItem->deadline = $request->item['deadline'] ?? Carbon::now()->addDays(7);
        $newItem->save();

        return $newItem;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @throws \Exception
     */
    public function update(Request $request, string $id)
    {
        $existingItem = Item::findOrFail($id);

        if (!$this->canModify($existingItem)) {
            throw new \Exception('You can not operate with this item.', 403);
        }

        //TODO here is an example validation but ideally we would validate all request data (in all endpoints)
        $request->validate([
            'status' => [new Enum(Status::class)],
        ]);

        if ($request->item['status'] === Status::UNCOMPLETED->value && !$this->canSetUncompleted($existingItem)) {
            throw new \Exception('You have to first set the task to ' . Status::COMPLETED->value, 403);
        }

        $existingItem->status = $request->item['status'];
        $existingItem->completed_at = $request->item['status'] === Status::COMPLETED ? Carbon::now() : null;
        $existingItem->save();

        return $existingItem;
    }

    /**
     * Remove the specified resource from storage.
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        $existingItem = Item::findOrFail($id);

        if (!$this->canModify($existingItem)) {
            throw new \Exception('You can not operate with this item.', 403);
        }

        $existingItem->delete();

        return "The item has been deleted.";
    }
}
