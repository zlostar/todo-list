<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\Item;
use App\Enums\Status;

trait ItemRulesTrait
{
    private function canModify(Item $item) : bool
    {
        return !$this->isExpired($item) && !$this->isDisabled($item);
    }

    private function canSetUncompleted(Item $item) : bool
    {
        return $this->isCompleted($item);
    }

    private function isCompleted(Item $item) : bool
    {
        return $item->status === Status::COMPLETED->value;
    }

    private function isExpired(Item $item) : bool
    {
        return $item->deadline < Carbon::now();
    }

    private function isDisabled(Item $item) : bool
    {
        return $item->status === Status::DISABLED->value;
    }
}
