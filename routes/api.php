<?php

use App\Http\Controllers\TodoListController;
use App\Http\Controllers\ItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//TODO SECURE ENDPOINTS WITH KEYCLOAK AND JWT for example
Route::get('/{todo_list_id}/items', [ItemController::class, 'index']);
Route::post('/{todo_list_id}/item/store', [ItemController::class, 'store']);
Route::prefix('/item')->group(function() {
    Route::put('/{id}', [ItemController::class, 'update']);
    Route::delete('/{id}', [ItemController::class, 'destroy']);
});

Route::get('/todos', [TodoListController::class, 'index']);
Route::prefix('/todo')->group(function() {
    Route::post('/store', [TodoListController::class, 'store']);
    Route::put('/{id}', [TodoListController::class, 'update']);
    Route::delete('/{id}', [TodoListController::class, 'destroy']);
});
