<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\TodoList;
use Illuminate\Database\Eloquent;

class TodoListControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_application_returns_a_successful_response(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_the_get_todos_returns_a_successful_response(): void
    {
        TodoList::factory()->create();

        $response = $this->call('GET', '/api/todos');

        $content = json_decode($response->getContent());

        $response->assertStatus(200);
        $this->assertCount(1, $content);
        $this->assertCount(0, $content[0]->items);
    }

    public function test_the_delete_todo_returns_a_successful_response(): void
    {
        $todo = TodoList::factory()->create();

        $response = $this->call('DELETE', '/api/todo/' . $todo->value('id'));

        $response->assertStatus(200);

        $response = $this->call('DELETE', '/api/todo/' . $todo->value('id'));

        $response->assertStatus(404);
    }
}
