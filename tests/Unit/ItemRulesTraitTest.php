<?php
use App\Models\Item;
use App\Enums\Status;

use PHPUnit\Framework\TestCase;

class ItemRulesTraitTest extends TestCase
{
    use \App\Traits\ItemRulesTrait;

    public function test_is_disabled(): void
    {
        $item = new Item();
        $item->setStatus(Status::DISABLED->value);

        $this->assertTrue($this->isDisabled($item));
    }

    public function test_is_not_disabled(): void
    {
        $item = new Item();
        $item->setStatus(Status::OPEN->value);

        $this->assertFalse($this->isDisabled($item));
    }

    public function test_is_expired(): void
    {
        $item = new Item();
        $item->setDeadline(\Carbon\Carbon::now()->subDays(7));

        $this->assertTrue($this->isExpired($item));
    }

    public function test_is_not_expired(): void
    {
        $item = new Item();
        $item->setDeadline(\Carbon\Carbon::now()->addDays(7));

        $this->assertFalse($this->isExpired($item));
    }

    public function test_is_completed(): void
    {
        $item = new Item();
        $item->setStatus(Status::COMPLETED->value);

        $this->assertTrue($this->isCompleted($item));
    }

    public function test_is_not_completed(): void
    {
        $item = new Item();
        $item->setStatus(Status::OPEN->value);

        $this->assertFalse($this->isCompleted($item));
    }

    public function test_can_set_uncompleted(): void
    {
        $item = new Item();
        $item->setStatus(Status::COMPLETED->value);

        $this->assertTrue($this->canSetUncompleted($item));
    }

    public function test_can_not_set_uncompleted(): void
    {
        $item = new Item();
        $item->setStatus(Status::OPEN->value);

        $this->assertFalse($this->canSetUncompleted($item));
    }

    public function test_can_modify(): void
    {
        $item = new Item();
        $item->setStatus(Status::OPEN->value);
        $item->setDeadline(\Carbon\Carbon::now()->addDays(7));

        $this->assertTrue($this->canModify($item));
    }

    public function test_can_not_modify(): void
    {
        $item = new Item();
        $item->setStatus(Status::OPEN->value);
        $item->setDeadline(\Carbon\Carbon::now()->subDays(7));

        $this->assertFalse($this->canModify($item));
    }
}
